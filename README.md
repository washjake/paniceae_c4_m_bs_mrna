# Analysis of C4 mesophyl and bundle sheath mRNA seq data in the tribe Paniceae (Poaceae) #


### Workflow ###

1) Clean, trim, and downsample reads:

* for file in *1.fastq; do java -jar ~/Trimmomatic-0.38/trimmomatic-0.38.jar PE ${file} ${file%1.fastq}2.fastq -baseout ${file%_R1.fastq}.fastq LEADING:15 TRAILING:15 SLIDINGWINDOW:4:20 MINLEN:36; done

* Trim reads to 50bp and downsample so all samples have same number of total input reads.

2) Map reads to Sorguhm bicolor genome using trinity and express:

* export PATH=/programs/express:$PATH
* /programs/trinityrnaseq-Trinity-v2.8.4/util/align_and_estimate_abundance.pl --thread_count <num>  --transcripts Sbicolor_454_v3.1.1.transcript.fa --seqType fq --left <reads_1P.fastq> --right <reads_2P.fastq> --est_method eXpress --aln_method bowtie2 --bowtie2_eXpress "--end-to-end --very-sensitive --rdg 3,1 --rfg 3,1 --score-min L,-0.9,-0.9 " --output_dir <out_name> --SS_lib_type RF &> <out_name.out>

3) Run DEseq2

* run: 3_convert_express_to_HTseq.ipynb script
* run: 3b_DEseq2_MS_BS.R script

4) Blast transcripts agains C4 gene list to find genes for use in figures and smale scale analyses.

* run: 4_Find_highest_expressed_genes_by_list_protien.ipynb

5) Build heatmaps

* run: 5_Extract_log2_padj_for_all_species.ipynb

6) Build plots for simple M and BS comparison figures

* run 6_Extract_TPMs_by_list_for_all_species.ipynb

7) Perform whole leaf vs bundle sheath comparisons within S. indica (C3 species).

* On the command line type:
* R
* BiocManager::install(c('edgeR','limma','DESeq2','ctc','Biobase','qvalue','goseq','fastcluster'))
* install.packages('gplots')
* install.packages('ape')
* install.packages("argparse")
Exit R

* export TRINITY_HOME=~/trinityrnaseq-Trinity-v2.8.4/
* cd ../data/
* mkdir S_indica_DE_analysis
* cd S_indica_DE_analysis
* $TRINITY_HOME/util/abundance_estimates_to_matrix.pl --est_method eXpress --gene_trans_map none ../express_output/S_vilivoides_*.xprs
* $TRINITY_HOME/util/abundance_estimates_to_matrix.pl --est_method eXpress --gene_trans_map Sbicolor_454_v3.1.1.gene_trans_map1.txt ../express_output/S_vilivoides_*.xprs

* $TRINITY_HOME/Analysis/DifferentialExpression/run_DE_analysis.pl --matrix eXpress.gene.counts.matrix --method DESeq2 --samples_file samples_file.txt
* cd DESeq2.XXXXX.dir/
* $TRINITY_HOME/Analysis/DifferentialExpression/analyze_diff_expr.pl --matrix ../eXpress.gene.TMM.EXPR.matrix -P 0.001 -C 2  --samples ../samples_file.txt

Re-run with GO terms if desired

* run 7_Make_Go_annotations_list.ipynb
* $TRINITY_HOME/util/misc/fasta_seq_length.pl Sbicolor_454_v3.1.1.transcript.fa > Sbicolor_454_v3.1.1.transcript.fa.seq_lens
* $TRINITY_HOME/util/misc/TPM_weighted_gene_length.py --gene_trans_map Sbicolor_454_v3.1.1.gene_trans_map1.txt --trans_lengths Ref_genome_files/Sbicolor_454_v3.1.1.transcript.fa.seq_lens --TPM_matrix eXpress.isoform.TPM.not_cross_norm > Sbicolor_454_v3.1.transcript.fa.gene_lengths.txt
* $TRINITY_HOME/Analysis/DifferentialExpression/analyze_diff_expr.pl --matrix ../eXpress.gene.TMM.EXPR.matrix -P 0.001 -C 2  --samples samples_file.txt --examine_GO_enrichment --GO_annots Sbicolor_454_v3.1.1.GO_annotations_genes.txt --gene_lengths Sbicolor_454_v3.1.transcript.fa.gene_lengths.txt

8) Compare BS from C4 species to BS of S.indica.

* R
* BiocManager::install(c('edgeR','limma','DESeq2','ctc','Biobase','qvalue','goseq','fastcluster'))
* install.packages('gplots')
* install.packages('ape')
* install.packages("argparse")

* $TRINITY_HOME/util/abundance_estimates_to_matrix.pl --est_method eXpress --gene_trans_map Sbicolor_454_v3.1.1.gene_trans_map1.txt ../express_output/*BS*.xprs

* $TRINITY_HOME/Analysis/DifferentialExpression/run_DE_analysis.pl --matrix eXpress.gene.counts.matrix --method DESeq2 --samples_file samples_file.txt --reference_sample S_vilivoides_BS
* $TRINITY_HOME/Analysis/DifferentialExpression/analyze_diff_expr.pl --matrix eXpress.gene.TMM.EXPR.matrix -P 0.001 -C 2  --samples samples_file.txt --reference_sample S_vilivoides_BS

Re-run analyze_diff_expr.pl with GO terms obtained from phytozome

* $TRINITY_HOME/util/misc/TPM_weighted_gene_length.py --gene_trans_map Sbicolor_454_v3.1.1.gene_trans_map1.txt --trans_lengths Sbicolor_454_v3.1.1.transcript.fa.seq_lens --TPM_matrix eXpress.isoform.TPM.not_cross_norm > Sbicolor_454_v3.1.transcript.fa.gene_lengths.txt
* $TRINITY_HOME/Analysis/DifferentialExpression/analyze_diff_expr.pl --matrix eXpress.gene.TMM.EXPR.matrix -P 0.001 -C 2  --samples samples_file.txt --examine_GO_enrichment --GO_annots Sbicolor_454_v3.1.1.GO_annotations_genes.txt --gene_lengths Sbicolor_454_v3.1.transcript.fa.gene_lengths.txt
