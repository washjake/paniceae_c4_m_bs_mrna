#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os,sys
import pandas as pd


# In[7]:


for file1 in os.listdir("../express_output/"):
    if file1[-13:]!="_results.xprs": continue
    print file1
    tmp_tbl=pd.read_csv("../express_output/"+file1, delimiter="\t")
    tmp_tbl.sort_values(by="target_id", inplace=True)
    out = tmp_tbl[["target_id", "tot_counts"]]
    out.to_csv("htseq_format/"+file1[:-5]+".htseq_format", header=False, index=False, sep="\t")


# In[6]:





# In[ ]:




