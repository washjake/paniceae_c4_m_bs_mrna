#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


annotations = pd.read_csv("../data/Sbicolor_454_v3.1.1.annotation_info.txt", sep="\t")


# In[3]:


#make file with all go terms in it. Can be uploaded to https://yeastmine.yeastgenome.org/yeastmine/bag.do to make list of go term names
gos = annotations["GO"].dropna().tolist()
gos = list(set([item for sublist in [x.split(",") for x in gos] for item in sublist]))
gos = pd.DataFrame(gos)
gos.to_csv("../data/Sbicolor_454_v3.1.1.annotation_go_terms.txt", sep="\t",header=False, index=False)
print(len(gos))


# In[4]:


go_genes = annotations[["locusName","GO"]].copy()
go_genes.dropna(inplace=True)
go_genes.drop_duplicates(inplace=True)
print(len(go_genes), len(go_genes["locusName"].unique()))
#go_genes[go_genes["locusName"].duplicated(keep=False)]
new=[]
for gene in go_genes["locusName"].unique():
    if len(go_genes[go_genes["locusName"]==gene]) > 1:
        gos = go_genes[go_genes["locusName"]==gene]["GO"].tolist()
        gos = list(set([item for sublist in [x.split(",") for x in gos] for item in sublist]))
        gos = ",".join(gos)
        new.append([gene,gos])
    else:
        new.append([gene,go_genes[go_genes["locusName"]==gene]["GO"].iloc[0]])
new = pd.DataFrame(new)
print(len(new))
new.to_csv("../data/Sbicolor_454_v3.1.1.GO_annotations_genes.txt", sep="\t", header=False, index=False)


# In[5]:


go_trans = annotations[["transcriptName","GO"]].copy()
go_trans.dropna(inplace=True)
print(len(go_trans))
go_trans.to_csv("../data/Sbicolor_454_v3.1.1.GO_annotations.txt", sep="\t", header=False, index=False)


# In[ ]:




