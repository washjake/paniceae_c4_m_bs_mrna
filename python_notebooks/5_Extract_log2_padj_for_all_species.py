#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os,sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# In[2]:


#outfile=open("../data/heatmap/combined_log2_padj_all_spp.csv","w")
Deseq_dir="../data/DEseq2/"
list_file="../data/sp_gene_list_dir/Final_list_for_DESeq_extr_numbered_1_cut.txt"


# In[3]:


list1={}
#add in gene(s) from annotation
list1["Sobic.003G236800.1"]="BASS2"
log2_dic={}
padj_dic={}

for line in open(list_file,"r"):
    list1[line.split("\t")[0]]= line.split("\t")[1].split("\n")[0]

files = [x for x in os.listdir(Deseq_dir) if x[-18:]=="_DESeq_results.csv"]
for file1 in files:
    print(file1)
    for line1 in open(Deseq_dir+file1):
        if line1[:13]=='"","baseMean"': continue
        if line1.split('",')[0].split('"')[1] not in list1: continue
        if list1[line1.split('",')[0].split('"')[1]] in log2_dic:
            log2_dic[list1[line1.split('",')[0].split('"')[1]]]+=","+line1.split(",")[2]
            padj_dic[list1[line1.split('",')[0].split('"')[1]]]+=","+line1.split(",")[6].split("\n")[0]
        else:
            log2_dic[list1[line1.split('",')[0].split('"')[1]]]=line1.split(",")[2]
            padj_dic[list1[line1.split('",')[0].split('"')[1]]]=line1.split(",")[6].split("\n")[0]
#print(log2_dic)
#print(padj_dic)

#compile above into dataframe
log2 = pd.DataFrame(log2_dic, index=["log2_"]).T
log2 = log2["log2_"].str.split(",", expand=True)
log2.columns = [x.split("_")[0] for x in files]
log2 = log2.astype("float").T
log2.index = log2.index.str.replace(".",". ")

padj = pd.DataFrame(padj_dic, index=["padj_"]).T
padj = padj["padj_"].str.split(",", expand=True)
padj.columns = ["padj_"+x.split("_")[0] for x in files]
padj = padj.astype("float").T
padj.index = padj.index.str.replace(".",". ")
padj.index = padj.index.str[5:]

def pvalues(x):
    #print(x)
    if x < 0.001: return "***"
    if x < 0.01: return "**"
    if x < 0.05: return "*"
    if x >= 0.05: return ""
    #return x
padj = padj.applymap(pvalues)

#re-order
log2 = log2.loc[['S. italica', 'U. fusca', 'P. hallii', 'D. eriantha','S. vilivoides']]
padj = padj.loc[['S. italica', 'U. fusca', 'P. hallii', 'D. eriantha','S. vilivoides']]


# In[4]:


def generate_MS_BS_lists(genes, final_TPM_list):
    TPM_list = final_TPM_list.copy()
    TPM_list["gene_name"] = TPM_list.index.to_series().str.split(".", expand=True)[0]
    order = {}
    for x in range(0, len(genes)):
         order[genes[x]]=x
    genes = TPM_list[TPM_list["gene_name"].isin(genes)].copy()
    genes["order"] = genes[["gene_name"]].replace(order)["gene_name"].to_list()
    genes = genes.sort_values("order")
    genes = genes.index.tolist()
    return genes


# In[5]:


#genes desired in plot and in order desired
exclude = ["DiT2", "PPT.1", "Ala-AT.1", "PEPC.2"]
MS_genes = ["CA","PEPC", "ASP-AT","DIT1","NADP-MDH", "Ala-AT",'BASS2','NHD', "PPDK","PPT"] #'OMT1'
MS_genes = [x for x in generate_MS_BS_lists(MS_genes, log2.T) if x not in exclude]
BS_genes = ["ASP-AT",'DIC', "PCK", "NADP-ME", "NAD-MDH","NAD-ME","Ala-AT","RBCS","RBCSACT"]
BS_genes = [x for x in generate_MS_BS_lists(BS_genes, log2.T) if x not in exclude]


# In[42]:


#build heatmap
#MS_genes = ["CA","PEPC.1","NADP-MDH.1","NADP-MDH","PPDK"]
#BS_genes = ["NADP-ME","NAD-ME","PCK","NAD-MDH","RBCSACT","RBCS"]

fig, ax = plt.subplots(figsize=(34,10))
sns.heatmap(log2[MS_genes+BS_genes], annot=padj[MS_genes+BS_genes], fmt="s", ax=ax, linewidths=.5, center=0, cmap="YlGnBu")
plt.tick_params(axis="both",bottom=False, top=False, left=False, right=False)
plt.rcParams.update({'font.size': 35})
plt.tight_layout()
plt.savefig("../data/heatmap/M_BS_heatmap.svg")
plt.show()


# In[ ]:





# In[ ]:




