#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import StrMethodFormatter


# In[2]:


list_file="../data/sp_gene_list_dir/Final_list_for_DESeq_extr_numbered_1_cut.txt"
#list_file="../data/sp_gene_list_dir/Final_list_for_DESeq_extr_numbered.txt"


# In[3]:


#bring in genes name to gene identifier mapping
gene_dict={}
for line in open(list_file,"r"):
    gene_dict[line.split("\t")[0]]=line.split("\t")[1].split("\n")[0]
#add any additional genes from sorghum annotation
gene_dict["Sobic.003G236800.1"]="BASS2"
#gene_dict["Sobic.005G125400.1"] = "DIC.1"
#gene_dict["Sobic.009G081700.2"] = "DIC.2"
#gene_dict["Sobic.007G037600.1"] = "DIC.3"
gene_dict


# In[4]:


#bring in TPM values
trans_dict={}
exp_files=[x for x in os.listdir("../data/express_output/") if x[-5:]==".xprs"]
for file in exp_files:
    TPM_dict={}
    sample=file[:-13]
    tmp = pd.read_csv("../data/express_output/"+file, delimiter="\t", index_col=1)["tpm"]
    tmp = tmp.loc[gene_dict.keys()]
    tmp.rename(index=gene_dict, inplace=True)
    trans_dict[sample]=tmp.to_dict()
#print trans_dict


# In[5]:


#create compiled dataframe and save to file
final_TPM_list = pd.DataFrame(trans_dict)
final_TPM_list.index.name = "gene"
final_TPM_list.to_csv("../data/simple_ms_bs_fig/Final_list_for_DESeq2_extr_numbered_1_cut"+"_all_TPM.txt", sep="\t")


# In[6]:


###CREATE Plots for figure###


# In[7]:


def generate_MS_BS_lists(genes, final_TPM_list):
    TPM_list = final_TPM_list.copy()
    TPM_list["gene_name"] = TPM_list.index.to_series().str.split(".", expand=True)[0]
    order = {}
    for x in range(0, len(genes)):
         order[genes[x]]=x
    genes = TPM_list[TPM_list["gene_name"].isin(genes)].copy()
    genes["order"] = genes[["gene_name"]].replace(order)["gene_name"].to_list()
    genes = genes.sort_values("order")
    genes = genes.index.tolist()
    return genes


# In[8]:


#genes desired in plot and in order desired
MS_genes = ["CA","PEPC", "ASP-AT","DIT1","NADP-MDH", "Ala-AT",'BASS2','NHD', "PPDK","PPT"] #'OMT1'
MS_genes = generate_MS_BS_lists(MS_genes, final_TPM_list)
BS_genes = ["ASP-AT",'DIC', "PCK", "NADP-ME", "NAD-MDH","NAD-ME","Ala-AT","RBCS","RBCSACT"]
BS_genes = generate_MS_BS_lists(BS_genes, final_TPM_list)


# In[9]:


#prepare list for subdeviding easily
final_TPM_list = final_TPM_list.T
sample_labels = final_TPM_list.index.to_series().str.split("_", expand=True)
final_TPM_list["species"] = sample_labels[0]+"_"+sample_labels[1]
final_TPM_list["cell_type"] = sample_labels[2]
final_TPM_list["rep"] = sample_labels[3]


# In[10]:


final_TPM_list


# In[11]:


def get_colors(main_colors, sng_sp_Ctype_stats):
    main_colors = pd.DataFrame(main_colors)#.from_dict(main_colors, orient='index').reset_index()
    main_colors.columns = ["enzyme", "color"]

    colors = pd.DataFrame(sng_sp_Ctype_stats.columns.tolist(), columns=["dup_names"])
    colors["enzyme"] = colors["dup_names"].str.split(".", expand=True)[0]
    colors = pd.merge(main_colors, colors, on="enzyme", how="right")
    colors.fillna('0.8', inplace=True)
    colors.index = colors["dup_names"]
    colors = colors["color"].to_dict()
    return colors


# In[12]:


def bar_plot_tpms(sng_sp_Ctype_stats, title, colors, figsize=(10,5)):
    #assign_colors
    for gene in [x for x in sng_sp_Ctype_stats.columns if x not in colors.keys()]:
        colors[gene]="gray"
    c = sng_sp_Ctype_stats.columns.to_series().apply(lambda x: colors[x])
    
    plt.rcParams.update({'font.size': 22})
    fig, ax = plt.subplots(figsize=figsize)
    ax.patch.set_alpha(0) #make back of plot transparent
    plt.bar(x=sng_sp_Ctype_stats.loc["mean"].index, height=sng_sp_Ctype_stats.loc["mean"]/1000,
            yerr=sng_sp_Ctype_stats.loc["stderr"]/1000, color=c, edgecolor = "black", capsize=6)
    plt.ylabel("TPM x 1,000")
    plt.gca().yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}'))
    plt.title(title)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    #ax.spines["bottom"].set_visible(False)
    ax.margins(0.01, 0.03)
    plt.xticks(rotation = 90)
    plt.tick_params(axis='x', bottom=False, top=False)
    #plt.box(on=None)
    plt.show()


# In[13]:


exclude = ["DiT2", "PPT.1", "Ala-AT.1", "PEPC.2"]

#go through species and cell type and create plots
#for species in final_TPM_list["species"].unique():
for species in ['S_italica', 'U_fusca', 'P_hallii', 'D_eriantha','S_vilivoides']:
    for cell_type in final_TPM_list[final_TPM_list["species"] == species]["cell_type"].unique():
    #for cell_type in ["MS","BS"]:   
        
        #create frame with single species and cell type
        sng_sp_cell_type = final_TPM_list[(final_TPM_list["species"]==species) & (final_TPM_list["cell_type"]==cell_type)].copy()
        
        #filter and order genes based on list
        if sng_sp_cell_type["cell_type"].unique()[0] == "MS":
            sng_sp_cell_type = sng_sp_cell_type[["species","cell_type","rep"] + 
                                                [x for x in MS_genes if x in sng_sp_cell_type.columns]]
        elif sng_sp_cell_type["cell_type"].unique()[0] == "BS":
            sng_sp_cell_type = sng_sp_cell_type[["species","cell_type","rep"] + 
                                                [x for x in BS_genes if x in sng_sp_cell_type.columns]]
        elif sng_sp_cell_type["cell_type"].unique()[0] == "WL":
            sng_sp_cell_type = sng_sp_cell_type[["species","cell_type","rep"] + 
                                                [x for x in MS_genes if x in sng_sp_cell_type.columns]]
        
        #calculate mean, std, stderror
        sng_sp_Ctype_stats = pd.DataFrame([sng_sp_cell_type.mean(), sng_sp_cell_type.std(),
                                           sng_sp_cell_type.count(numeric_only=True)], index=["mean", "std", "n"])
        sng_sp_Ctype_stats.loc["stderr"] = sng_sp_Ctype_stats.loc["std"]/np.sqrt(sng_sp_Ctype_stats.loc["n"])
        sng_sp_Ctype_stats.columns.name=""
        #exlude any that are not useful
        sng_sp_Ctype_stats = sng_sp_Ctype_stats[[x for x in sng_sp_Ctype_stats.columns if x not in exclude]]
        
        #create plots
        main_colors = [["NADP-MDH", (63/256,100/256,246/256)], 
                       ["NADP-ME", (63/256,100/256,246/256)], 
                       ["PCK", (255/256,250/256,83/256)],
                       ["NAD-ME", (56/256,126/256,34/256)],
                       ["NAD-MDH", (56/256,126/256,34/256)]]
        colors = get_colors(main_colors, sng_sp_Ctype_stats)
        print(species, cell_type)    
        #bar_plot_tpms(sng_sp_Ctype_stats, title=species+" "+cell_type, colors=colors, figsize=(9,6))
        bar_plot_tpms(sng_sp_Ctype_stats, title="", colors=colors, figsize=(9,6))


# In[14]:


def get_sng_cell_data_stats(species, cell_type, final_TPM_list, MS_genes, BS_genes, exclude, filter_cell_type=True):
    #create frame with single species and cell type
    sng_sp_cell_type = final_TPM_list[(final_TPM_list["species"]==species) & (final_TPM_list["cell_type"]==cell_type)].copy()
    
    if filter_cell_type:
        #filter and order genes based on list
        if sng_sp_cell_type["cell_type"].unique()[0] == "MS":
            sng_sp_cell_type = sng_sp_cell_type[["species","cell_type","rep"] + 
                                                    [x for x in MS_genes if x in sng_sp_cell_type.columns]]
        elif sng_sp_cell_type["cell_type"].unique()[0] == "BS":
            sng_sp_cell_type = sng_sp_cell_type[["species","cell_type","rep"] + 
                                                    [x for x in BS_genes if x in sng_sp_cell_type.columns]]
        elif sng_sp_cell_type["cell_type"].unique()[0] == "WL":
            sng_sp_cell_type = sng_sp_cell_type[["species","cell_type","rep"] + 
                                                    [x for x in MS_genes if x in sng_sp_cell_type.columns]]
    else:
        sng_sp_cell_type = sng_sp_cell_type[["species","cell_type","rep"] + 
                                                    [x for x in MS_genes+[z for z in BS_genes if z not in MS_genes] if x in sng_sp_cell_type.columns]]
        
    #calculate mean, std, stderror
    sng_sp_Ctype_stats = pd.DataFrame([sng_sp_cell_type.mean(), sng_sp_cell_type.std(),
                                       sng_sp_cell_type.count(numeric_only=True)], index=["mean", "std", "n"])
    sng_sp_Ctype_stats.loc["stderr"] = sng_sp_Ctype_stats.loc["std"]/np.sqrt(sng_sp_Ctype_stats.loc["n"])
    sng_sp_Ctype_stats.columns.name=""
    #exlude any that are not useful
    sng_sp_Ctype_stats = sng_sp_Ctype_stats[[x for x in sng_sp_Ctype_stats.columns if x not in exclude]]
    
    #create plots
    main_colors = [["NADP-MDH", (63/256,100/256,246/256)], 
                       ["NADP-ME", (63/256,100/256,246/256)], 
                       ["PCK", (255/256,250/256,83/256)],
                       ["NAD-ME", (56/256,126/256,34/256)],
                       ["NAD-MDH", (56/256,126/256,34/256)]]
    colors = get_colors(main_colors, sng_sp_Ctype_stats)
    
    return sng_sp_Ctype_stats, sng_sp_cell_type, colors


# In[15]:


#play with S. indica
species = 'S_vilivoides'
cell_type = "WL"
WL_sng_sp_cell_type_stats, WL_sng_sp_cell_type, colors = get_sng_cell_data_stats(species, cell_type, final_TPM_list,
                                                                         MS_genes, BS_genes, exclude, filter_cell_type=False)
bar_plot_tpms(WL_sng_sp_cell_type_stats, title="", colors=colors, figsize=(9,6))


# In[16]:


species = 'S_vilivoides'
cell_type = "BS"
BS_sng_sp_cell_type_stats, BS_sng_sp_cell_type, colors = get_sng_cell_data_stats(species, cell_type, final_TPM_list,
                                                                         MS_genes, BS_genes, exclude, filter_cell_type=False)
bar_plot_tpms(BS_sng_sp_cell_type_stats, title="", colors=colors, figsize=(9,6))


# In[ ]:





# In[ ]:





# In[ ]:




