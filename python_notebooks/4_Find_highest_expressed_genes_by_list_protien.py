#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd


# In[2]:


protein_ref="../data/Sbicolor_454_v3.1.1.protein_primaryTranscriptOnly.fa"
C4_genes="../data/C4_gene_list.txt.fa"
gene_counts_dir="../data/express_output/"
sp_gene_list_dir = "../data/sp_gene_list_dir"


# In[3]:


#create blastdb and blast for C4 genes
mbdb = "makeblastdb -in "+protein_ref+" -parse_seqids -dbtype prot -out "+protein_ref+"_db_for_blast.fasta"
print mbdb
os.system(mbdb)
#print "Running blast analysis"
run_blast = "psiblast -db "+protein_ref+"_db_for_blast.fasta -query "+C4_genes+" -evalue 0.00001 -out "+protein_ref+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq -outfmt '6 qseqid evalue bitscore sseqid sstart send pident sseq'"
print run_blast
os.system(run_blast)
os.system("rm "+protein_ref+"_db_for_blast.fasta*")
#print "Sorting blast results"
sort = "sort -k1,1 "+protein_ref+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq > "+protein_ref+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq_sorted.txt"
print sort
os.system(sort)


# In[4]:


#bring in original non-sorted blast file
blast_res = pd.read_csv(protein_ref+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq",delimiter="\t", 
                         header=None, names=["name","evalue","bitscore","Contig_blast_id","sstart","send","pident","sseq"])
blast_res = blast_res[blast_res["pident"]>85.0] #Discard hits with < 85% identity
blast_res["Contig_blast_id"] = blast_res["Contig_blast_id"].str[:-2]

#for each S. bicolor gene. determine which gene name is the closest match and remove others.
#essentially annotate each sorghum gene with only its best hit.
blast_res = blast_res.sort_values(["Contig_blast_id", "bitscore"], ascending=False)#.sort_values("Contig_blast_id")
blast_res = blast_res.drop_duplicates("Contig_blast_id").reset_index(drop=True)

#label duplicate genes as .1, .2, etc.
#pd.set_option('display.max_rows', 2000)
blast_res = blast_res.sort_values(["name","bitscore"], ascending=False).reset_index(drop=True)
blast_res["name_dup"] = blast_res["name"]
mask = blast_res["name"].duplicated(keep=False)
blast_res.loc[mask,"name_dup"] += "."+blast_res.groupby("name_dup").cumcount().add(1).astype(str)


# In[5]:


### Go through list and keep only the highest expressed version from each species.
#Remove any genes that are not the highest expressed in at least one species


# In[6]:


#get file list for expression
file_list = pd.DataFrame([x for x in os.listdir(gene_counts_dir) if x[-5:]==".xprs"],
                         columns=["file_name"])
file_list["species"] = file_list["file_name"].str.split("_", expand=True)[0]+"_"+file_list["file_name"].str.split("_", expand=True)[1]


# In[7]:


all_merged = []
for species in file_list["species"].unique():
    
    #bring in data for all reps and tissue types of a single species
    sp_data=[]
    for file1 in file_list[file_list["species"]==species]["file_name"]:
        #print file1
        sp_data.append(pd.read_csv(gene_counts_dir+file1, delimiter="\t"))
    sp_data = pd.concat(sp_data)
    sp_data = sp_data[["target_id","tpm"]]
    
    #combine with blast results and remove any genes not in blast results. Keep only highest value from the replicates
    merged = pd.merge(blast_res[["name","name_dup","Contig_blast_id"]],
                      sp_data,left_on="Contig_blast_id", right_on="target_id", how="left")
    merged.sort_values(by=["name_dup",'tpm'], ascending=False, inplace=True)
    merged = merged.drop_duplicates(subset="name_dup").reset_index(drop=True)
    merged[["Contig_blast_id","name","name_dup","tpm"]].to_csv(sp_gene_list_dir+"/"+species+"_gene_name_list.txt",
                                                               header=False,index=False, sep="\t")
    tmp = merged[["Contig_blast_id","name","name_dup","tpm"]].copy()
    tmp.index=pd.MultiIndex.from_frame(tmp[["Contig_blast_id","name","name_dup"]])
    tmp = tmp.rename(columns={"tpm":species})
    tmp = tmp.drop(columns=["Contig_blast_id","name","name_dup"])
    all_merged.append(tmp)
    
    print(species, len(merged["Contig_blast_id"].unique()), len(merged["name_dup"].unique()))
all_merged = pd.concat(all_merged, sort=False, axis=1)
print(all_merged.shape)


# In[8]:


#Combine lists from all species into one non-redundant list with numbers for genes represented differently in different species
#only keep genes that are the top in at least one species
Sp_CUTOFF=1 #number of species required to keep gene
def find_top_gene(series1):
    return (series1.groupby("name").transform(max) == series1).astype(int)

top_gene_by_sp = all_merged.apply(find_top_gene).copy()
top_gene_by_sp = top_gene_by_sp[top_gene_by_sp.sum(axis=1) >= Sp_CUTOFF]

#create and output file
out_table = []
for row in top_gene_by_sp.index:
    out_table.append([row[0], row[2], [x[:3] for x in top_gene_by_sp.loc[row][top_gene_by_sp.loc[row]==1].index]])

#save to file
pd.DataFrame(out_table).to_csv(sp_gene_list_dir+"/"+"Final_list_for_DESeq_extr_numbered_"+str(Sp_CUTOFF)+"_cut.txt",
                               sep="\t", index=False, header=False)


# In[ ]:




